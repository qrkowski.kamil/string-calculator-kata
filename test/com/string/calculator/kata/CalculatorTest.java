package com.string.calculator.kata;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    void addingEmptyStringShouldReturnZero() {
        //Given
        String inputNumbers = "";

        //When
        int result = Calculator.add(inputNumbers);

        //Then
        assertEquals(0, result);
    }

    @Test
    void addingSingleNumberShouldReturnPassedNumber() {
        //Given
        String inputNumbers = "1";

        //When
        int result = Calculator.add(inputNumbers);

        //Then
        assertEquals(1, result);
    }

    @Test
    void addingMultipleNumbersShouldReturnPassedNumbersSum() {
        //Given
        String inputNumbers = "1,3,2";

        //When
        int result = Calculator.add(inputNumbers);

        //Then
        assertEquals(6, result);
    }

    @Test
    void addingNegativeNumbersShouldFail() {
        //Given
        String inputNumbers = "1,-3,2";

        //Then
        assertThrows(
                Calculator.NegativesNotAllowedException.class,
                //When
                () -> Calculator.add(inputNumbers)
        );
    }

    @Test
    void addingMultipleNumbersSeparatedByNewLineShouldReturnPassedNumbersSum() {
        //Given
        String inputNumbers = "2" + System.lineSeparator() + "3,2";

        //When
        int result = Calculator.add(inputNumbers);

        //Then
        assertEquals(7, result);
    }

    @Test
    void addingNumbersSeparatedByCustomDelimitersShouldReturnValidSum() {
        //Given
        String inputNumbers = "//[:]" + System.lineSeparator() + "2:3:2:4";

        //When
        int result = Calculator.add(inputNumbers);

        //Then
        assertEquals(11, result);
    }

    @Test
    void addingNumbersSeparatedByMultipleCustomDelimitersShouldReturnValidSum() {
        //Given
        String inputNumbers = "//[:][;][&&]" + System.lineSeparator() + "2;3:2&&4";

        //When
        int result = Calculator.add(inputNumbers);

        //Then
        assertEquals(11, result);
    }

    @Test
    void addingNumbersGreaterThanAllowedMaxValueShouldBeIgnored() {
        //Given
        String inputNumbers = "1,3,1001";

        //When
        int result = Calculator.add(inputNumbers);

        //Then
        assertEquals(4, result);
    }
}