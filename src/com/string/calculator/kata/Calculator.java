package com.string.calculator.kata;

import java.io.Serial;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Calculator {

    private static final String DEFAULT_DELIMITER_REGEX = ",|\\r?\\n";
    private static final Pattern DELIMITER_DEFINITION_PATTERN = Pattern.compile("\\[([^\\]]+)\\]");
    private static final int MAX_ALLOWED_NUMBER_VALUE = 1000;
    private static final String DELIMITERS_SECTION_START = "//";

    static int add(String numbers) {
        Objects.requireNonNull(numbers);

        if (numbers.isBlank()) {
            return 0;
        }

        List<Integer> splittedNumbers = splitNumbers(numbers);
        ValidationResult validationResult = validate(splittedNumbers);

        if(!validationResult.isOk()) {
            throw new NegativesNotAllowedException(validationResult.invalidInput());
        }

        return splittedNumbers.stream()
                .filter(number -> number <= MAX_ALLOWED_NUMBER_VALUE)
                .reduce(0, Integer::sum);
    }

    private static List<Integer> splitNumbers(String numbers) {
        return Arrays.stream(removeDelimiterPrefix(numbers).split(establishDelimiters(numbers), -1))
                .map(Integer::parseInt)
                .toList();
    }

    private static String removeDelimiterPrefix(String input) {
        if (input.startsWith(DELIMITERS_SECTION_START)) {
            return input.substring(getDelimiterSection(input).length() +1);
        }
        return input;
    }

    private static String establishDelimiters(String input) {
        if (input.startsWith(DELIMITERS_SECTION_START)) {
            String delimiterSection = getDelimiterSection(input);
            delimiterSection = delimiterSection.replace(DELIMITERS_SECTION_START, "");
            return String.join("|", extractDelimiters(delimiterSection));
        }
        return DEFAULT_DELIMITER_REGEX;
    }

    private static String getDelimiterSection(String input) {
        String[] splittedInput = input.split("\\r?\\n");
        if (splittedInput.length < 2) {
            throw new IllegalArgumentException(
                    String.format(
                            "Invalid input data provided. \"%s\" should match '//([.+])\\n+d+' or 'd+' pattern",
                            input
                    )
            );
        }

        return splittedInput[0];
    }

    private static List<String> extractDelimiters(String delimiterSection) {
        List<String> delimiters = new ArrayList<>();

        Matcher singleDelimiterMatcher = DELIMITER_DEFINITION_PATTERN.matcher(delimiterSection);
        while (singleDelimiterMatcher.find()) {
            if (singleDelimiterMatcher.groupCount() >= 1) {
                delimiters.add(singleDelimiterMatcher.group(1));
            }
        }
        return delimiters;
    }

    private static ValidationResult validate(List<Integer> numbers) {
        List<Integer> negativeNumbers = numbers.stream()
                .filter(number -> number < 0)
                .toList();

        if (!negativeNumbers.isEmpty()) {
            return ValidationResult.negative(negativeNumbers);
        }
        return ValidationResult.positive();
    }

    private record ValidationResult(boolean isOk, List<Integer> invalidInput) {
        static ValidationResult positive() {
            return new ValidationResult(true, Collections.emptyList());
        }
        static ValidationResult negative(List<Integer> invalidNumbers) {
            return new ValidationResult(false, invalidNumbers);
        }
    }

    static class NegativesNotAllowedException extends RuntimeException {
        @Serial
        private static final long serialVersionUID = -2986467725774637352L;

        public NegativesNotAllowedException(List<Integer> negativeValues) {
            super("Negatives not allowed:" + negativeValues);
        }
    }
}
