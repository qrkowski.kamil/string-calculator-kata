package com.string.calculator.kata;

public class Main {

    public static void main(String[] args) {
        if (args.length == 1) {
            System.out.println(Calculator.add(args[0]));
        }
    }
}
